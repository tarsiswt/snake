package br.edu.fat.al.tap;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

enum Direction {
    NORTH, SOUTH, EAST, WEST
}

public class Main {

    // DONE: desenhar o corpo da cobra de acordo com as suas posições
    // DONE: desenhar maçã na sua posição
    // DONE: animar movimento da cobra
    // DONE: reagir ao input do usuário
    // DONE: quando a maçã for comida, movê-la para uma posição nova aleatória
    // TODO: quando a maçã for comida, movê-la para uma posição nova *válida*
    // TODO: implementar regras para game over (colisão com o corpo da cobra)
    // TODO: implementar cálculo pontuação
    // TODO: permitir que as dimensões do tabuleiro seja configurável
    // TODO: permitir que a velocidade da cobra seja configurável
    // TODO: implementar regras para game over (colisão com as paredes)
    // TODO: implementar exibição da pontuação (score)
    // TODO: implementar condição de vitória
    public static void main(String[] args) {
        World world = new World();

        // --

        GamePanel panel = new GamePanel(world);
        GameFrame frame = new GameFrame(panel);

        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(2);
        scheduledExecutorService.scheduleWithFixedDelay(() -> world.stepForward(), 1000, 1000, TimeUnit.MILLISECONDS);
        scheduledExecutorService.scheduleAtFixedRate(() -> frame.repaint(), 1000, 17, TimeUnit.MILLISECONDS);
    }
}

class World {
    Snake snake;
    Apple apple;
    Random random = new Random();

    public World() {
        Position position = new Position(5, 5);
        this.apple = new Apple(position);

        Deque<Position> body = new ArrayDeque<>();
        body.addFirst(new Position(1, 1));
        body.addLast(new Position(1, 0));

        this.snake = new Snake(body, Direction.SOUTH);
    }

    public void stepForward() {
        // recalcular posições do corpo da cobra
        Position newHead = snake.body.removeLast();
        Position currentHead = snake.body.getFirst();
        if (snake.direction.equals(Direction.SOUTH)) {
            newHead.x = currentHead.x;
            newHead.y = currentHead.y + 1;
        } else if (snake.direction.equals(Direction.NORTH)) {
            newHead.x = currentHead.x;
            newHead.y = currentHead.y - 1;
        } else if (snake.direction.equals(Direction.WEST)) {
            newHead.x = currentHead.x - 1;
            newHead.y = currentHead.y;
        } else if (snake.direction.equals(Direction.EAST)) {
            newHead.x = currentHead.x + 1;
            newHead.y = currentHead.y;
        }
        snake.body.addFirst(newHead);

        if (newHead.equals(apple.position)) {
            System.out.println("AppleEaten");
            apple.position.x = random.nextInt(10);
            apple.position.y = random.nextInt(10);
        }
        System.out.println("SnakeMoved");
    }
}

class GamePanel extends JPanel implements KeyListener {
    World world;

    public GamePanel(World world) {
        this.world = world;
        setPreferredSize(new Dimension(300, 300));
        setFocusable(true);
        setVisible(true);
        addKeyListener(this);
    }

    @Override
    protected void paintComponent(Graphics g) {
        clear(g);
        paintApple(g);
        paintSnake(g);
    }

    private void paintSnake(Graphics g) {
        g.setColor(Color.DARK_GRAY);
        for (Position position : world.snake.body) {
            g.fillRect(position.x * 30, position.y * 30, 30, 30);
        }
    }

    private void clear(Graphics g) {
        super.paintComponent(g);
    }

    private void paintApple(Graphics g) {
        g.setColor(Color.RED);
        g.fillRect(world.apple.position.x * 30, world.apple.position.y * 30, 30, 30);
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == KeyEvent.VK_DOWN) {
            world.snake.direction = Direction.SOUTH;
        } else if (keyEvent.getKeyCode() == KeyEvent.VK_UP) {
            world.snake.direction = Direction.NORTH;
        } else if (keyEvent.getKeyCode() == KeyEvent.VK_LEFT) {
            world.snake.direction = Direction.WEST;
        } else if (keyEvent.getKeyCode() == KeyEvent.VK_RIGHT) {
            world.snake.direction = Direction.EAST;
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {

    }
}

class GameFrame extends JFrame {

    public GameFrame(JPanel panel) {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300, 300);
        getContentPane().add(panel);
        setVisible(true);
        pack();
    }
}

class Snake {
    Deque<Position> body; // Double-Ended Queue
    Direction direction;

    public Snake(Deque<Position> body, Direction direction) {
        this.body = body;
        this.direction = direction;
    }
}

class Apple {
    Position position;

    public Apple(Position position) {
        this.position = position;
    }
}

class Position {
    int x, y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return x == position.x &&
               y == position.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
